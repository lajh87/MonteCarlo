library(shiny)
library(monteCarlo)
ui <- monteCarloUI("mc")

server <- function(input, output, session) {
  shiny::callModule(monteCarlo, "mc")

}

shinyApp(ui, server)
